package com.mymvc.model

import com.google.gson.annotations.SerializedName

/**
 * Created by david on 11/02/16.
 */
data class PollItem(var name: String, @SerializedName("pic_url") var picUrl: String)