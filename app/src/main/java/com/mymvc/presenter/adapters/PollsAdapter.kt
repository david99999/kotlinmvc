package com.mymvc.presenter.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.mymvc.R
import com.mymvc.model.PollItem
import com.mymvc.presenter.interfaces.MainPresenter
import com.mymvc.view.holders.GenericViewHolder
import com.mymvc.view.holders.PollItemView
import java.util.*

/**
 * Created by david on 11/02/16.
 */
class PollsAdapter(var items: ArrayList<PollItem>, var listener: MainPresenter) : RecyclerView.Adapter<GenericViewHolder>() {

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parentView: ViewGroup?, position: Int): GenericViewHolder? {
        var holder = LayoutInflater.from(parentView?.context).inflate(R.layout.poll_item_view, parentView, false) as PollItemView
        holder.setClickListeners(listener)
        return GenericViewHolder(holder)
    }

    override fun onBindViewHolder(holder: GenericViewHolder?, position: Int) =
            (holder?.itemView as PollItemView).setData(items[position])
}
