package com.mymvc.presenter.interfaces

/**
 * Created by david on 15/02/16.
 */
interface LoadFromServerInteractor{
    fun loadData(listener: LoadProcessListener)
    fun releaseSubscription()
}