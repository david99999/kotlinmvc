package com.mymvc.presenter.interfaces

import android.view.View
import com.mymvc.model.PollItem
import java.util.*

/**
 * Created by david on 15/02/16.
 */
interface MainView {
    fun showLoading()
    fun hideLoading()
    fun showMessage(view: View)
    fun setItems(items: ArrayList<PollItem>)
    fun showError(message: String)
}