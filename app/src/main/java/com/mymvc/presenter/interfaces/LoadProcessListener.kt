package com.mymvc.presenter.interfaces

/**
 * Created by david on 12/02/16.
 */
interface LoadProcessListener {
    fun OnDataLoaded(data: Any)
    fun OnError(exception: Throwable)
    fun OnDestroy()
}