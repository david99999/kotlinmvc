package com.mymvc.presenter.interfaces

/**
 * Created by david on 15/02/16.
 */
interface MainPresenter {
    fun loadItems()
    fun viewClicked(view: Any)
}