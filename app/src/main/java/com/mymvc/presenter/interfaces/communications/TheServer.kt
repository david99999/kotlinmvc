package com.mymvc.presenter.interfaces.communications

import android.content.Context
import com.mymvc.BuildConfig
import com.mymvc.presenter.utils.GET_ALL
import com.mymvc.presenter.utils.SERVER_URL
import com.mymvc.model.PollItem
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import rx.Observable
import java.util.*

/**
 * Created by david on 11/02/16.
 */
interface TheServer {

    @GET(GET_ALL)
    fun getPolls(): Observable<ArrayList<PollItem>>

    companion object {

        lateinit var instance: TheServer

        fun init(context: Context, url: String = SERVER_URL) {
            var builder = OkHttpClient().newBuilder()

            if (BuildConfig.DEBUG) {
                var interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY;
                builder.addInterceptor(interceptor);
            }
            builder.cache(Cache(context.cacheDir, 10 * 1024 * 1024)) //10Mb

            val restAdapter = Retrofit.Builder()
                    .baseUrl(url)
                    .client(builder.build())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            instance = restAdapter.create(TheServer::class.java)
        }
    }
}