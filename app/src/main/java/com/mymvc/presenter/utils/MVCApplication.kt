package com.mymvc.presenter.utils

import android.app.Application
import com.mymvc.presenter.interfaces.communications.TheServer

/**
 * Created by david on 13/02/16.
 */
class MVCApplication: Application(){
    override fun onCreate() {
        super.onCreate()
        TheServer.init(this)
    }
}