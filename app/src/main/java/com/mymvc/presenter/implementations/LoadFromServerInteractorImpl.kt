package com.mymvc.presenter.implementations;

import android.util.Log
import com.mymvc.model.PollItem
import com.mymvc.presenter.interfaces.LoadFromServerInteractor
import com.mymvc.presenter.interfaces.LoadProcessListener
import com.mymvc.presenter.interfaces.communications.TheServer
import com.mymvc.presenter.utils.LOG_TAG
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.*

/**
 * Created by david on 15/02/16.
 */
class LoadFromServerInteractorImpl : LoadFromServerInteractor {

    lateinit var subscription: Subscription

    override fun loadData(listener: LoadProcessListener) {
        subscription = TheServer.instance.getPolls()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext { items -> listener.OnDataLoaded(items) }
                .onErrorReturn { it: Throwable? ->
                    listener.OnError(it ?: Exception("no desc"))
                    ArrayList<PollItem>()
                }
                .subscribe()
    }

    override fun releaseSubscription() {
        if (subscription.isUnsubscribed) {
            Log.e(LOG_TAG, "request cancelled")
            subscription.unsubscribe()
        }
    }
}
