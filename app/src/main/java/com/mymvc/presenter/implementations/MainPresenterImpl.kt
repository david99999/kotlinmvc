package com.mymvc.presenter.implementations

import android.view.View
import com.mymvc.model.PollItem
import com.mymvc.presenter.interfaces.LoadFromServerInteractor
import com.mymvc.presenter.interfaces.LoadProcessListener
import com.mymvc.presenter.interfaces.MainPresenter
import com.mymvc.presenter.interfaces.MainView
import java.util.*

/**
 * Created by david on 15/02/16.
 */
class MainPresenterImpl(var mainView: MainView) : MainPresenter, LoadProcessListener {
    var interactor: LoadFromServerInteractor

    init {
        interactor = LoadFromServerInteractorImpl()
    }

    override fun loadItems() {
        mainView.showLoading()
        interactor.loadData(this)
    }

    override fun OnDataLoaded(data: Any) {
        mainView.hideLoading()
        mainView.setItems(data as ArrayList<PollItem>)
    }

    override fun OnError(exception: Throwable) {
        mainView.hideLoading()
        mainView.showError(exception.message ?: "no desc")
    }

    override fun viewClicked(view: Any) {
        mainView.showMessage(view as View)
    }

    override fun OnDestroy() {
        interactor.releaseSubscription()
    }
}