package com.mymvc.view

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.mymvc.R
import com.mymvc.model.PollItem
import com.mymvc.presenter.adapters.PollsAdapter
import com.mymvc.presenter.implementations.MainPresenterImpl
import com.mymvc.presenter.interfaces.MainView
import kotlinx.android.synthetic.main.content_main.*
import java.util.*

class MainActivity : AppCompatActivity(), MainView {
    override fun showLoading() {
        rv_main.visibility = View.GONE
        pb_loading.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        rv_main.visibility = View.VISIBLE
        pb_loading.visibility = View.GONE
    }

    override fun showMessage(view: View) {
        when (view) {
            is ImageView -> showToast("Click en Imagen")
            is TextView -> showToast("Click en Texto")
        }
    }

    override fun setItems(items: ArrayList<PollItem>) {
        rv_main.adapter = PollsAdapter(items, presenter)
    }

    override fun showError(message: String) {
        rv_main.visibility = View.GONE
        pb_loading.visibility = View.GONE
        showToast(message, Toast.LENGTH_LONG)
    }

    lateinit var presenter: MainPresenterImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        val fab = findViewById(R.id.fab) as FloatingActionButton
        presenter = MainPresenterImpl(this)
        fab.setOnClickListener {
            rv_main.adapter = null
            presenter.loadItems()
        }
        presenter.loadItems()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.OnDestroy()
    }
}
