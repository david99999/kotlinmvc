package com.mymvc.view.holders

import android.content.Context
import android.support.v7.widget.CardView
import android.util.AttributeSet
import com.mymvc.model.PollItem
import com.mymvc.presenter.interfaces.MainPresenter
import com.mymvc.view.loadPic
import kotlinx.android.synthetic.main.poll_item_view.view.*

/**
 * Created by david on 14/02/16.
 */
class PollItemView(context: Context, attrs: AttributeSet) : CardView(context, attrs) {
    fun setData(item: PollItem) {
        tv_poll_name.text = item.name
        iv_poll_pic.loadPic(item.picUrl)
    }

    fun setClickListeners(listener: MainPresenter) {
        iv_poll_pic.setOnClickListener { view -> listener.viewClicked(view) }
        tv_poll_name.setOnClickListener { view -> listener.viewClicked(view) }
    }
}