package com.mymvc.view.holders;

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Created by david on 14/02/16.
 */
class GenericViewHolder(view: View) : RecyclerView.ViewHolder(view)
