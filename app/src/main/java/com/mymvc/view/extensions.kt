package com.mymvc.view

import android.content.Context
import android.widget.ImageView
import android.widget.Toast
import com.mymvc.R
import com.squareup.picasso.Picasso

/**
 * Created by david on 14/02/16.
 */

fun Context.showToast(message: String, length: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, length).show()
}

fun ImageView.loadPic(url: String) {
    Picasso.with(context).load(url).error(R.mipmap.ic_launcher).into(this)
}